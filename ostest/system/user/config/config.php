<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['index_page'] = '';
$config['is_system_on'] = 'y';
$config['multiple_sites_enabled'] = 'n';
$config['show_ee_news'] = 'n';
// ExpressionEngine Config Items
// Find more configs and overrides at
// https://docs.expressionengine.com/latest/general/system_configuration_overrides.html

$config['app_version'] = '5.2.2';
$config['encryption_key'] = '0571972eab8254dd4e492b5089e3d1f57963b427';
$config['session_crypt_key'] = 'bbeae6a2c5425dbdced5439e0f223303fbf17bbc';
$config['database'] = array(
	'expressionengine' => array(
		'hostname' => 'localhost',
		'database' => 'orangfo8_ee5',
		'username' => 'orangfo8_ee5',
		'password' => 'sunShine2k19',
		'dbprefix' => 'osl_',
		'port'     => ''
	),
);

$config['resizer_target'] = '/images/sized/';

// EOF