/*!
 * Bootstrap v4.0.0 (https://getbootstrap.com)
 * Copyright 2011-2018 The Bootstrap Authors
 * Copyright 2011-2018 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 */

@import 'tests/vendor/jquery-1.9.1.min.js';
@import 'src/alert.js';
@import 'src/button.js';
@import 'src/carousel.js';
@import 'src/collapse.js';
@import 'src/dropdown.js';
@import 'src/modal.js';
@import 'src/popover.js';
@import 'src/scrollspy.js';
@import 'src/tab.js';
@import 'src/tooltip.js';
@import 'src/util.js';
@import 'dist/alert.js';
@import 'dist/button.js';
@import 'dist/carousel.js';
@import 'dist/collapse.js';
@import 'dist/dropdown.js';
@import 'dist/modal.js';
@import 'dist/popover.js';
@import 'dist/scrollspy.js';
@import 'dist/tab.js';
@import 'dist/tooltip.js';
@import 'dist/util.js';